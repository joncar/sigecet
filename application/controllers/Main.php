<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
ob_start();
session_name('siaweb');
session_start();

class Main extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('h');
        $this->load->database();
        $this->sigesp = $this->load->database('sigesp',TRUE);
        $this->load->model('user');
        $this->load->model('bdsource');
        date_default_timezone_set('America/Asuncion');
    }
    
    public function getLdapConnection(){
        $ldapconn = ldap_connect('10.10.71.30') or die("No es posible conectar");
        ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
        if ($ldapconn) {
            $dn = 'cn=conexion,dc=fundabit,dc=gob,dc=ve';        
            $password = "fund4b1t14";
            ldap_bind($ldapconn,$dn,$password);
            return $ldapconn;
        }
        return false;
    }

    public function index() {
        $log = $this->user;
        if ($log->log)
            $this->loadView('panel');
        else
            $this->loadView('main');
    }

    public function success($msj) {
        return '<div class="alert alert-success">' . $msj . '</div>';
    }

    public function error($msj) {
        return '<div class="alert alert-danger">' . $msj . '</div>';
    }

    public function login() {
        if (!$this->user->log) {
            if (!empty($_POST['usuario']) && !empty($_POST['clave'])) {                
                if ($this->user->login($this->input->post('usuario', TRUE), $this->input->post('clave',TRUE))) {                                        
                    if (empty($_POST['redirect'])){
                        echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . site_url('panel') . '"</script>');
                    }
                    else{
                        echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . $_POST['redirect'] . '"</script>');
                    }
                } else{
                    $_SESSION['msj'] = $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
                }
            } else{
                $_SESSION['msj'] = $this->error('Debe completar todos los campos antes de continuar');
            }

            if (!empty($_SESSION['msj']))
                header("Location:" . base_url());
        } else
            header("Location:" . base_url());
    }

    function pages($titulo) {
        $titulo = urldecode(str_replace("-", "+", $titulo));
        if (!empty($titulo)) {
            $pagina = $this->db->get_where('paginas', array('titulo' => $titulo));
            if ($pagina->num_rows() > 0) {
                $this->loadView(array('view' => 'paginas', 'contenido' => $pagina->row()->contenido, 'title' => $titulo));
            } else
                $this->loadView('404');
        }
    }

    public function unlog() {
        $this->user->unlog();
        header("Location:" . site_url());
    }

    public function loadView($param = array('view' => 'main')) {
        if (is_string($param))
            $param = array('view' => $param);
        $this->load->view('template', $param);
    }

    public function loadViewAjax($view, $data = null) {
        $view = $this->valid_rules($view);
        $this->load->view($view, $data);
    }

    function error404() {
        $this->loadView(array('view' => 'errors/403'));
    }

    function sendMailer() {
        $this->load->library('mailer');
        $this->mailer->mail('joncar.c@gmail.com', 'Test', 'Test');
    }

}
