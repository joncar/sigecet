<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Main.php');
class Panel extends Main {
        CONST __DESARROLLADOR__ = 1;
        CONST __TUTOR__ = 2;
        CONST __CORDINADOR__ = 3;
        CONST __ADMINISTRADOR__ = 4;
        protected $primary_keys = array();
        public function __construct()
        {
                parent::__construct();
                $this->load->library('grocery_crud');
                $this->load->library('ajax_grocery_crud');                
                if(empty($_SESSION['user'])){
                    header("Location:".base_url());
                    die();
                }                
                if(!$this->user->hasAccess()){
                    throw  new Exception('<b>ERROR: 403<b/> Usted no posee permisos para realizar esta operaci贸n','403');
                    exit;
                }
        }
        
        public function loadView($param = array('view' => 'main'))
        {
            if(empty($_SESSION['user'])){
                header("Location:".base_url('main?redirect='.$_SERVER['REQUEST_URI']));
            }            
            else{
                if(!empty($param->output)){
                    $param->view = empty($param->view)?'panel':$param->view;
                    $param->crud = empty($param->crud)?'user':$param->crud;
                    $param->title = empty($param->title)?ucfirst($this->router->fetch_method()):$param->title;
                }
                parent::loadView($param);            
            }
        }
        
        protected function crud_function($x = '',$y = '',$controller = ''){
            $crud = new ajax_grocery_CRUD($controller);
            $crud->set_theme('bootstrap2');
            $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
            $crud->set_table($table);
            $crud->set_subject(ucfirst($this->router->fetch_method()));
            /*foreach($this->primary_keys as $p){
                $crud->set_primary_key($p[0],$p[1]);
            }*/
            if(!empty($this->norequireds)){
                $crud->norequireds = $this->norequireds;
            }
            $crud->required_fields_array();
            
            if(method_exists('panel',$this->router->fetch_method()))
             $crud = call_user_func(array('panel',$this->router->fetch_method()),$crud,$x,$y);
            return $crud;
        }  
        
        function set_primary_key($field,$table){
            $this->primary_keys[] = array($field,$table);
        }
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
