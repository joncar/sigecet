<a href="<?= base_url('control_inventario/admin/inventario') ?>" class="btn btn-info"><i class="fa fa-chevron-circle-left"></i> Volver a todos los articulos</a>
<div class="panel panel-default">
    <div class="panel-heading">
        Administrar movimientos para el articulo <b><?= $inventario->tipo_inventario ?></b> asignado al centro <b><?= $inventario->nombre_plantel  ?></b>
    </div>
    <div class="panel-body">
        <?= $output ?>
    </div>
</div>
