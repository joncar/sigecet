<?= $output ?>
<?php if($action=='add' || $action=='edit'): ?>
<link rel="stylesheet" href="<?= base_url('css/codemirror') ?>/codemirror.css">
<script src="<?= base_url('js/codemirror') ?>/codemirror.js"></script>
<script src="<?= base_url('js/codemirror') ?>/matchbrackets.js"></script>
<script src="<?= base_url('js/codemirror') ?>/htmlmixed.js"></script>
<script src="<?= base_url('js/codemirror') ?>/xml.js"></script>
<script src="<?= base_url('js/codemirror') ?>/javascript.js"></script>
<script src="<?= base_url('js/codemirror') ?>/css.js"></script>
<script src="<?= base_url('js/codemirror') ?>/clike.js"></script>
<script src="<?= base_url('js/codemirror') ?>/php.js"></script>
<script src="<?= base_url('js/codemirror') ?>/sql.js"></script>
<script>
    $(document).ready(function(){
       $("#field-tipo").change(function(){
            $(".CodeMirror ").remove();
            switch($(this).val()){
                case 'sql':
                    var myCodeMirror = CodeMirror.fromTextArea(document.getElementById('field-script'),{
                         lineNumbers: true,            
                         mode: "text/x-mariadb",
                         indentUnit: 1,
                         indentWithTabs: true,
                         extraKeys: {"Ctrl-Space": "autocomplete"},
                    });        
                break;
                case 'php':
                    var myCodeMirror = CodeMirror.fromTextArea(document.getElementById('field-script'),{
                         lineNumbers: true,            
                         mode: "application/x-httpd-php",
                         indentUnit: 1,
                         indentWithTabs: true,
                         extraKeys: {"Ctrl-Space": "autocomplete"},
                    });        
                break;
            }
       });       
       if($("#field-tipo").val()!==''){
           $("#field-tipo").trigger('change');
       }
    });
</script>
<?php endif ?>