<?php if($this->user->log): ?>
<div id="sidebar" class="sidebar responsive">
        <ul class="nav nav-list">
            <li class="active highlight">
                <a href="<?= site_url('panel') ?>">
                        <i class="menu-icon fa fa-tachometer"></i>
                        <span class="menu-text">Escritorio</span>
                </a>
                <b class="arrow"></b>
            </li>
             <!--- Alumnos --->
             <?php 
                    $menu = array(
                        'evaluacion'=>array('admin/centro'),
                        
                        'catalogo'=>array('admin/tipo_aire',                         
                            'admin/tipo_centro','admin/tipo_conectividad',
                            'admin/tipo_iluminacion','admin/tipo_inventario',
                            'admin/tipo_paredes','admin/tipo_piso',
                            'admin/tipo_puerta','admin/tipo_techo','admin/tipo_ventanas','admin/marca'),
                        
                        'control_inventario'=>array('admin/inventario'),
                        
                        'reportes' => array('rep/reportes'),
                        
                        'seguridad'=>array('grupos','funciones','usuario')
                    );
                    $menu = $this->user->filtrarMenu($menu);
                    $label = array(
                        'seguridad'=>array('Seguridad','fa fa-user-secret'),
                        'evaluacion'=>array('Evaluación','fa fa-id-card'),                       
                        'catalogo'=>array('Catálogo','fa fa-database'),
                        'control_inventario'=>array('Inventario','fa fa-cubes'),
                        'reporte'=>array('Reportes','fa fa-files-o'),
                        'tipo_aire'=>array('<i class="fa fa-clipboard"></i> Tipo de Aire'),
                        'tipo_centro'=>array('<i class="fa fa-clipboard"></i> Tipo de Centro'),
                        'tipo_conectividad'=>array('<i class="fa fa-clipboard"></i> Tipo de Conectividad'),                    
                        'tipo_iluminacion'=>array('<i class="fa fa-clipboard"></i> Tipo de Iluminación'),
                        'tipo_inventario'=>array('<i class="fa fa-clipboard"></i> Tipo de Inventario'),
                        'tipo_paredes'=>array('<i class="fa fa-clipboard"></i> Tipo de Paredes'),
                        'tipo_piso'=>array('<i class="fa fa-clipboard"></i> Tipo de Piso'),
                        'tipo_puerta'=>array('<i class="fa fa-clipboard"></i> Tipo de Puerta'),
                        'tipo_techo'=>array('<i class="fa fa-clipboard"></i> Tipo de Techo'),
                        'tipo_ventanas'=>array('<i class="fa fa-clipboard"></i> Tipo de Ventanas'),
                        'marca'=>array('<i class="fa fa-clipboard"></i> Marcas'),
                        'inventario'=>array('<i class="fa fa-list-alt"></i> Inventario'),
                        'reportes'=>array('<i class="fa fa-book"></i> Reportes'),
                        'centro'=>array('<i class="fa fa-university"></i> Centro'),                       



                    );
                    
                   
                 
             ?>
             <?php  echo getMenu($menu,$label); ?>            
        </ul>
       <div id="sidebar-collapse" class="sidebar-toggle sidebar-collapse">
            <i data-icon2="ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-left" class="ace-icon fa fa-angle-double-left"></i>
        </div>

        <script type="text/javascript">
                try{ace.settings.check('sidebar' , 'collapsed')
                ace.settings.sidebar_collapsed(true, true);
                }catch(e){}
        </script>
</div>
<?php endif ?>
