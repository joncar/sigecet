<?php

require_once APPPATH . '/controllers/Panel.php';

class Admin extends Panel {

    function __construct() {
        parent::__construct();
    }

    function inventario($x = '', $y = '') {
        $crud = $this->crud_function($x, $y);        
        $crud->set_relation('marca_id', 'marca', 'marca');
        $crud->display_as('centro_id', 'Centro');
        $crud->display_as('estatus_id', 'Estatus');
        $crud->display_as('tipo_inventario_id', 'Tipo de Inventario');
        $crud->display_as('marca_id', 'Marca')
             ->display_as('codigo_bien', 'Código de control de bien nacional')
             ->display_as('motivo_entrada', 'Motivo de entrada de inventario');
        $crud->set_rules('codigo_bien','Código de control de bien nacional','required|alpha_numeric|is_unique[inventario.codigo_bien]');
        if($crud->getParameters()!='list'){
            $crud->fields('centro_id','estatus_id','tipo_inventario_id','marca_id','serial_fabrica','codigo_bien','modelo','motivo_entrada','estado');
        }
        $crud->field_type('estado','dropdown',array('1'=>'Activo','2'=>'Dañado','3'=>'Perdido','4'=>'Robado'));
        $crud->edit_fields('tipo_inventario_id','marca_id','serial_fabrica','codigo_bien');
        $crud->set_rules('motivo_entrada','Motivo de entrada de inventario','required');
        $crud->callback_before_insert(function($post){
            get_instance()->motivo_entrada = $post['motivo_entrada'];
            unset($post['motivo_entrada']);
            return $post;
        });
        $crud->callback_after_insert(function($post,$primary){
            //Se realiza la entrada en el centro registrado
            get_instance()->db->insert('movimiento_inventario',array(
                'inventario_id'=>$primary,
                'centro_origen'=>$post['centro_id'],
                'centro_destino'=>$post['centro_id'],
                'tipo_movimiento'=>'E',
                'user_id'=>get_instance()->user->id,
                'fecha_movimiento'=>date("Y-m-d"),
                'motivo_movimiento'=>get_instance()->motivo_entrada
            ));
            return true;
        });
        //Perfiles de usuarios
        if(
            (
                in_array(self::__TUTOR__,$this->user->grupos) || 
                in_array(self::__CORDINADOR__,$this->user->grupos)
            ) && 
            (
                !in_array(self::__ADMINISTRADOR__,$this->user->grupos) &&
                !in_array(self::__DESARROLLADOR__,$this->user->grupos)
            )
        ){
            $crud->where('jdd33f329.estado_id',$this->user->estado_id);
            $crud->set_relation('centro_id', 'centro', 'nombre_plantel',array('estado_id'=>$this->user->estado_id));
        }else{
            $crud->set_relation('centro_id', 'centro', 'nombre_plantel');
        }
        $crud->add_action('<i class="fa fa-arrows-alt"></i> Movimientos','',base_url('control_inventario/admin/movimiento_inventario').'/');
        $crud->unset_delete()
             ->unset_export()
             ->unset_print()
             ->unset_read();
        $output = $crud->render();
        $this->loadView($output);
    }
    
    function movimiento_inventario($x = ''){
        if(is_numeric($x)){
            $this->id_inventario = $x;
            $this->db->select('inventario.centro_id, tipo_inventario.*, centro.nombre_plantel');
            $this->db->join('centro','centro.id = inventario.centro_id');
            $this->db->join('tipo_inventario','tipo_inventario.id = inventario.tipo_inventario_id');
            $inventario = $this->db->get_where('inventario',array('inventario.id'=>$x));
            if($inventario->num_rows()>0){
                $crud = $this->crud_function();
                $crud->set_subject('Movimiento');                
                $crud->display_as('centro_origen','Centro de origen del articulo')
                     ->display_as('centro_destino','Centro de destino del articulo')
                     ->display_as('user_id','Usuario que realizo el movimiento')
                     ->display_as('fecha_movimiento','Fecha del movimiento')
                     ->display_as('motivo_movimiento','Motivo del movimiento')
                     ->display_as('tipo_movimiento','Tipo del movimiento');
                
                $crud->field_type('inventario_id','hidden',$x)
                     ->field_type('user_id','hidden',$this->user->id)
                     ->field_type('fecha_movimiento','hidden',date("Y-m-d"))
                     ->field_type('centro_origen','hidden',$inventario->row()->centro_id);
                if($crud->getParameters()=='list'){
                    $crud->set_relation('centro_origen','centro','nombre_plantel')                         
                         ->set_relation('user_id','usuario','nombres_apellidos');
                    $crud->field_type('tipo_movimiento','dropdown',array('E'=>'Entrada','S'=>'Salida'));
                }
                $crud->callback_field('tipo_movimiento',function($val){
                    $this->db->order_by('id','DESC');
                    $ultimo_movimiento = get_instance()->db->get_where('movimiento_inventario',array('inventario_id'=>$this->id_inventario));
                    if($ultimo_movimiento->num_rows()>0){
                        $val = $ultimo_movimiento->row()->tipo_movimiento=='S'?'E':'S';
                    }else{
                        $val = 'E';
                    }
                    return form_input('tipo_movimiento_label',$val=='S'?'Salida':'Entrada','readonly').'<input type="hidden" name="tipo_movimiento" id="field-tipo_movimiento" value="'.$val.'">';
                })->callback_before_insert(function($post){
                    unset($post['tipo_inventario_label']);
                    return $post;
                })->callback_after_insert(function($post){
                    get_instance()->db->update('inventario',array('centro_id'=>$post['centro_destino']));
                    return true;
                });
                $crud->set_relation('centro_destino','centro','nombre_plantel');
                $crud->where('inventario_id',$x);
                $crud->columns('motivo_movimiento','fecha_movimiento','user_id','centro_origen','centro_destino','tipo_movimiento');        
                $crud->unset_delete()
                     ->unset_print()
                     ->unset_export()
                     ->unset_edit()
                     ->unset_read();
                $output = $crud->render();
                $output->crud = 'movimiento_inventario';
                $output->inventario = $inventario->row();
                $output->title = 'Movimiento de inventario';
                $this->loadView($output);
            }
        }
    }

}
?>            


