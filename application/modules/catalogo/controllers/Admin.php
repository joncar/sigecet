<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function tipo_aire($x = '',$y = ''){
            $crud = $this->crud_function($x,$y); 
            $crud->set_subject('Tipo de Aire');
            $crud->unset_delete();
            $output = $crud->render();
            $output ->title='Tipo de Aire';
            $this->loadView($output);

        }
        
//        function aire_acondicionado($x = '',$y = ''){
//            $crud = $this->crud_function($x,$y); 
//            $crud->set_subject('Aire Acondicionado');
//            $crud->unset_delete();
//            $output = $crud->render();
//            $output ->title='Aire Acondicionado';
//            $this->loadView($output);
//
//        }
        
       
        
         function tipo_centro($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);                        
            $output = $crud->render();
            $this->loadView($output);

        }
        
         function tipo_conectividad($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);                        
            $output = $crud->render();
            $this->loadView($output);

        }
        
         function tipo_iluminacion($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);                        
            $output = $crud->render();
            $this->loadView($output);

        }
        
         function tipo_inventario($x = '',$y = ''){
            $crud = $this->crud_function($x,$y); 
            $crud->field_type('equipo_mobiliario','enum',array(
                'Equipo',
                'Mobiliario'
            ));
            $output = $crud->render();
            $this->loadView($output);

        }
        
          function tipo_paredes($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);                        
            $output = $crud->render();
            $this->loadView($output);

        }
        
          function tipo_piso($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);                        
            $output = $crud->render();
            $this->loadView($output);

        }
        
          function tipo_puerta($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);                        
            $output = $crud->render();
            $this->loadView($output);

        }
        
          function tipo_techo($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);                        
            $output = $crud->render();
            $this->loadView($output);

        }
        
          function tipo_ventanas($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);                        
            $output = $crud->render();
            $this->loadView($output);

        }
        
         function marca($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);  
            if($crud->getParameters()=='add'){
                $crud->set_rules('marca','Marca','required|is_unique[marca.marca]');
            }
            //$crud->unset_delete();
            $output = $crud->render();           
            $this->loadView($output);

        }
      
               
    }
?>       

