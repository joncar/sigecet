<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Seguridad extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function grupos($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);            
            $crud->set_relation_n_n('funciones','funcion_grupo','funciones','grupo','funcion','{nombre}');
            $crud->field_type('lectura','true_false',array('0'=>'NO','1'=>'SI'));
            $crud->field_type('escritura','true_false',array('0'=>'NO','1'=>'SI'));
            $crud->add_action('<i class="fa fa-users"></i> Adm. Users','',base_url('seguridad/user_group').'/');
            $output = $crud->render();
            $this->loadView($output);
        }     
        
        function user_group($x = '',$y = ''){            
            if(!is_numeric($x)){
                $data = array();
                $ldapconn = $this->getLdapConnection();
                if($ldapconn){                    
                    $result = ldap_search($ldapconn,"ou=$y,ou=Usuarios,dc=fundabit,dc=gob,dc=ve", "(objectClass=person)");
                    $entries = ldap_get_entries($ldapconn, $result);
                    foreach($entries as $e){
                        $data[] = array('name'=>$e['givenname'][0],'cedula'=>$e['carlicense'][0]);
                    }                    
                }
                echo json_encode($data);
            }else{
                $crud = $this->crud_function('','');
                $crud->set_subject('Usuario');
                $crud->where('grupo',$x);
                $crud->field_type('grupo','hidden',$x);
                $crud->field_type('priority','hidden',1);
                $crud->field_type('user','hidden','');
                $crud->unset_edit()
                     ->unset_export()
                     ->unset_print()
                     ->unset_read();
                $crud->fields('OU','UID','user','grupo','priority');
                $crud->callback_field('UID',function($val,$row){                                       
                    return form_dropdown('',array(),'','id="field-uid" class="form-control chosen-select"');
                });
                $crud->callback_field('OU',function($val,$row){                    
                    $ldapconn = $this->getLdapConnection();
                    if($ldapconn){
                        $result = ldap_search($ldapconn,"dc=fundabit,dc=gob,dc=ve", "(objectClass=organizationalunit)");
                        $entries = ldap_get_entries($ldapconn, $result);
                        $data = array();
                        foreach($entries as $e){
                            $data[$e['ou'][0]] = $e['ou'][0];
                        }
                    }
                    return form_dropdown('',$data,'','id="field-ou" class="form-control chosen-select"');
                });
                $crud->set_rules('user','Usuario','required|integer|callback_validate_user_group');
                $crud = $crud->render();
                $crud->title = "Asignación de usuarios para el grupo";
                $crud->output = $this->load->view('user_group',array('output'=>$crud->output),TRUE);
                $this->loadView($crud);                
            }
        }
        
        function validate_user_group(){
            if($this->db->get_where('user_group',array('user'=>$_POST['user'],'grupo'=>$_POST['grupo']))->num_rows()>0){
                $this->form_validation->set_message('validate_user_group','EL usuario ya ha sido asignado a este grupo');
                return false;
            }
            return true;
        }
        
        function funciones($x = '',$y = ''){  
            $this->set_primary_key('id','funciones');
            $crud = $this->crud_function($x,$y);             
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function usuario($x = '',$y = ''){
            $this->norequireds = array('apellido_materno','foto');
            $crud = $this->crud_function($x,$y);  
            
            $crud->display_as('perfil_id','Perfil')
                 ->display_as('estado_id','Estado');
            
            $crud->field_type('clave','password')
                 ->field_type('cargo_id','hidden',1)
                 ->field_type('telefono','telefono',array('placeholder'=>'(0999)999-9999'));
            if($crud->getParameters()=='add'){                
                $crud->set_rules('cedula','Cedula','required|is_unique[usuario.cedula]');
            }
            $crud->callback_before_insert(function($post){
                $post['clave'] = md5($post['clave']);
                return $post;
            });
            $crud->callback_before_update(function($post,$primary){
                if(!empty($primary) && $this->db->get_where('usuario',array('id'=>$primary))->row()->clave!=$post['clave']){
                    $post['clave'] = md5($post['clave']);                
                }
                return $post;
            });
            $crud->columns('cedula','nombres_apellidos');
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function perfil($x = '',$y = ''){
            $this->as['perfil'] = 'usuario';
            $crud = $this->crud_function($x,$y);    
            $crud->where('id',$this->user->id);
            $crud->field_type('clave','password')
                 ->field_type('perfil_id','invisible')
                 ->field_type('usuario','hidden',1)
                 ->field_type('cargo_id','invisible');
            $crud->display_as('perfil_id','Perfil')
                 ->display_as('estado_id','Estado');
            $crud->callback_before_update(function($post,$primary){
                if(!empty($primary) && $this->db->get_where('usuario',array('id'=>$primary))->row()->clave!=$post['clave'] || empty($primary)){
                    $post['clave'] = md5($post['clave']);                
                }
                return $post;
            });
            $crud->callback_after_update(function($post,$id){
                $this->user->login_short($id);
            });
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function user_insertion($post,$id = ''){
            if(!empty($id)){
                $post['pass'] = $this->db->get_where('usuario',array('id'=>$id))->row()->clave!=$post['clave']?md5($post['clave']):$post['clave'];
            }
            else $post['pass'] = md5($post['pass']);
            return $post;
        }
    }
?>
