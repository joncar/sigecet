<?= $output ?>
<?php echo $this->load->view('predesign/chosen',array(),TRUE) ?>
<script>
    $("#field-ou").change(function(){
        $.post('<?= base_url('seguridad/user_group/get_users') ?>/'+$(this).val(),{},function(data){
            data = JSON.parse(data);
            $("#field-uid").html('');
            for(var i in data){
                $("#field-uid").append('<option value="'+data[i]['cedula']+'">'+data[i]['name']+'</option>');
            }
            $("#field-uid").chosen().trigger('liszt:updated');
        });
    });
    
    $(document).on('change','#field-uid',function(){
        $("#field-user").val($(this).val());
    });
</script>
