<?= $output ?>
<script>
    $(document).on('ready',function(){        
        $("#field-direccion").change(function(){
            if($(this).val()!==''){
                searchDireccion($(this).val());
            }
        });
        $("#field-codigo_plantel").change(function(){
            $.post('<?= base_url('evaluacion/ajax/getPlantel') ?>',{codigo:$(this).val()},function(data){
                data = JSON.parse(data);
                if(data.length>0){
                    data = data[0];
                    $("#field-nombre_plantel").val(data.nombre);
                    $("#field-estado_id").val(data.estado_id);
                    $("#field-municipio_id").val(data.municipio_id);
                    $("#field-parroquia_id").val(data.parroquia_id);                  
                    $("#field-direccion").val(data.direccion);
                    $("#field-tlf_plantel").val(data.telefono_fijo);
                    $("#field-tlf_director").val(data.telefono);
                    $("#field-nombre_director").val(data.nomapedir);
                    $("#field-cedula_director").val(data.cedula);
                    $("#field-ano_creacion").val(data.annio_fundado);
                    $("#field-mapa").val('('+data.latitud+','+data.longitud+')');
                    $("select").chosen().trigger('liszt:updated');
                } 
                 else {
                    $("#field-nombre_plantel").val('');
                    $("#field-estado_id").val('');
                    $("#field-municipio_id").val('');
                    $("#field-parroquia_id").val('');
                    $("#field-direccion").val('');
                    $("#field-tlf_plantel").val('');
                    $("select").chosen().trigger('liszt:updated');
                }
            });
        });
    });
    
    $(document).on("change","#field-estado_id",function(){
        $.post('<?= base_url('evaluacion/ajax/getMunicipios') ?>/'+$(this).val(),{},function(data){
            $("#field-municipio_id").html('');
            data = JSON.parse(data);
            $("#field-municipio_id").append('<option value="">Seleccione un Municipio</option>');
            for(var i in data){
                $("#field-municipio_id").append('<option value="'+data[i].codmun+'">'+data[i].denmun+'</option>');
            }
            $("#field-municipio_id").chosen().trigger('liszt:updated');
        });
    });
    
    $(document).on("change","#field-municipio_id",function(){
        $.post('<?= base_url('evaluacion/ajax/getParroquias') ?>/'+$(this).val(),{},function(data){
            $("#field-parroquia_id").html('');
            data = JSON.parse(data);
            $("#field-municipio_id").append('<option value="">Seleccione una Parroquia</option>');
            for(var i in data){
                $("#field-parroquia_id").append('<option value="'+data[i].codpar+'">'+data[i].denpar+'</option>');
            }
            $("#field-parroquia_id").chosen().trigger('liszt:updated');
        });
    });
</script>
