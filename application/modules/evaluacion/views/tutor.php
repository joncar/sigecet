<!--<a href="'.base_url('evaluacion/admin/evaluacion').'" class="btn btn-info">Volver a la evaluación</a>-->

<?= $output ?>

<script>
    $(document).on('ready',function(){
        $("#field-cedula").change(function(){
            $.post('<?= base_url('evaluacion/ajax/getTutor') ?>',{cedula:$(this).val()},function(data){
                data = JSON.parse(data);
                if(data.length>0){
                    data = data[0];
                    $("#field-nombres").val(data.nomper);
                    $("#field-apellidos").val(data.apeper);
                }
            });
        });
    });
</script>