<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Ajax extends Panel{
        function __construct() {
            parent::__construct();
            $this->load->library('form_validation');
        }
        
        function getMunicipios($val){
            if(!empty($val)){
                $this->db->group_by('codmun');
                $plantel = $this->sigesp->get_where('sigesp_municipio',array('codest'=>$val));
                if($plantel->num_rows()>0){
                    $planteles = array();
                    foreach($plantel->result() as $p){
                        if($p->codmun!='---'){
                            $planteles[] = $p;
                        }
                    }
                    echo json_encode($planteles);
                }else{

                    echo '[]';
                }
            }else{

                echo '[]';
            }
        }
        function getParroquias($val){
            if(!empty($val)){
                $this->db->group_by('codpar');
                $plantel = $this->sigesp->get_where('sigesp_parroquia',array('codmun'=>$val));
                if($plantel->num_rows()>0){
                    $planteles = array();
                    foreach($plantel->result() as $p){
                        if($p->codpar!='---'){
                            $planteles[] = $p;
                        }
                    }
                    echo json_encode($planteles);
                }else{

                    echo '[]';
                }
            }else{

                echo '[]';
            }
        }
        
        function getPlantel(){            
            $this->form_validation->set_rules('codigo','Codigo','required');
            if($this->form_validation->run()){
                $plantel = $this->db->get_where('plantel_gesesc',array('cod_plantel'=>$this->input->post('codigo')));
                if($plantel->num_rows()>0){
                    $planteles = array();
                    foreach($plantel->result() as $p){
                        $planteles[] = $p;
                    }
                    echo json_encode($planteles);
                }else{
                    
                    echo '[]';
                }
            }else{
                echo '[]';
            }
        }
        
        function getTutor(){            
            $this->form_validation->set_rules('cedula','Cedula','required');
            if($this->form_validation->run()){
                $plantel = $this->db->get_where('v_personal',array('cedper'=>$this->input->post('cedula')));
                if($plantel->num_rows()>0){
                    $planteles = array();
                    foreach($plantel->result() as $p){
                        $planteles[] = $p;
                    }
                    echo json_encode($planteles);
                }else{
                    echo '[]';
                }
            }else{
                echo '[]';
            }
        }
        
       
    }
?>
