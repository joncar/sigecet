<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function get_centro($x){
            $centro = $this->db->get_where('evaluacion',array('id'=>$x));
            if($centro->num_rows()>0){
                $centro = $centro->row()->centro_id;
            }else{
                $centro = '';
            }
            return $centro;
        }
          ///prueba para git 
        function centro($x = '',$y = ''){            
            $crud = $this->crud_function($x,$y);                        
            $crud->display_as('tipo_centro_id','Tipo de Centro');
            $crud->display_as('estado_id','Estado');
            $crud->display_as('municipio_id','Municipio');
            $crud->display_as('parroquia_id','Parroquia');
            $crud->display_as('parroquia_id','Parroquia');
            $crud->display_as('mapa','Coordenadas Geográficas');
            $crud->display_as('nombre_plantel','Nombre del Plantel');
            $crud->display_as('codigo_plantel','Código del Plantel o Institución');
            $crud->display_as('ano_creacion','Año de Creación');
            $crud->display_as('tlf_director','Telefono del Director');
            $crud->display_as('tlf_plantel','Telefono del Plantel');
            $crud->display_as('tlf_otro_contacto','Telefono del Otro Contacto');
            $crud->display_as('ano_dotacion','Año de Dotación');
            $crud->display_as('codigo_centro','Código del Centro Tecnologíco');
            $crud->field_type('laboratorio_computacion','enum',array(
                'Si',
                'No'
            ));
            $crud->field_type('nivel_educativo','enum',array(
                'Inicial',
                'Primaria',
                'Media',
                'Inicial - Primaria',
                'Primaria - Media',
                'Inicial - Primaria - Media'
            ));            
            $crud->field_type('posee_personal_informatica','enum',array(
                'Si',
                'No'
            ));
            $crud->field_type('coordenada_latitud','hidden');
            $crud->field_type('coordenada_longitud','hidden');
            $crud->field_type('dependencia','enum',array(
                'Nacional',
                'Municipal',
                'Estadal',
                'AVEC',
                'Autónoma',
                'Privada'
            ));
            $crud->field_type('estatus_centro','enum',array(
                'Operativo',
                'No Operativo'
            ));
            $crud->field_type('estado_id','input');            
            $crud->field_type('municipio_id','input');
            $crud->field_type('parroquia_id','input');
            $crud->callback_field('estado_id',function($val){
                $estados = $this->sigesp->get('sigesp_estados');
                $data = array();
                foreach($estados->result() as $d){
                    $data[$d->codest] = $d->desest;
                }
                return form_dropdown('estado_id',$data,$val,'id="field-estado_id" class="chosen-select"');
            });
            $crud->callback_field('municipio_id',function($val){
                return form_dropdown('municipio_id',array(),$val,'id="field-municipio_id" class="chosen-select form-control"');
            });
            $crud->callback_field('parroquia_id',function($val){                
                return form_dropdown('parroquia_id',array(),$val,'id="field-parroquia_id" class="chosen-select form-control"');
            }); 
            
            $crud->callback_column('parroquia',function($val,$row){                
                return $this->sigesp->get_where('sigesp_parroquia',array('codest'=>$row->estado_id,'codmun'=>$row->municipio_id,'codpar'=>$row->parroquia_id))->row()->denpar;
            }); 
            
            $crud->callback_column('municipio',function($val,$row){
                return $this->sigesp->get_where('sigesp_municipio',array('codest'=>$row->estado_id,'codmun'=>$row->municipio_id))->row()->denmun;
            });
            
            $crud->callback_column('estado',function($val,$row){
                return $this->sigesp->get_where('sigesp_estados',array('codest'=>$row->estado_id))->row()->desest;
            });
            $crud->field_type('tlf_plantel','telefono',array('placeholder'=>'(0999)999-9999'));
            $crud->field_type('tlf_director','telefono',array('placeholder'=>'(0999)999-9999'));
            $crud->field_type('tlf_otro_contacto','telefono',array('placeholder'=>'(0999)999-9999'));
            $crud->field_type('mapa','mask',array('placeholder'=>'(00.000000,00.000000)','mask'=>'(00.000000,00.000000)'));
            if(
                    (
                        in_array(self::__TUTOR__,$this->user->grupos) || 
                        in_array(self::__CORDINADOR__,$this->user->grupos)
                    ) && 
                    (
                        !in_array(self::__ADMINISTRADOR__,$this->user->grupos) &&
                        !in_array(self::__DESARROLLADOR__,$this->user->grupos)
                    )
            ){
                $crud->where('centro.estado_id',$this->user->estado_id);
                $crud->field_type('estado_id','hidden',$this->user->estado_id);
            }
            
            $crud->unset_back_to_list();
            
            $crud->columns('id','tipo_centro_id','estado','municipio','parroquia','nombre_plantel','codigo_plantel');
            $crud->set_lang_string('insert_success_message','Sus datos fueron almacenados correctamente, <script>document.location.href="'.base_url('evaluacion/admin/evaluacion').'/add/{id}"</script>');
            $crud->unset_delete()->unset_read()->unset_export()->unset_print();
            $crud->unset_searchs('estado','municipio','parroquia');
            $crud->add_action('<i class="fa fa-file"></i> Evaluaciones','',base_url('evaluacion/admin/evaluacion').'/');
            $crud->add_action('<i class="fa fa-print"></i> Inventario','',base_url('reportes/rep/verReportes/6/html/centro').'/');
            $output = $crud->render();
            $output->output = $this->load->view('centro',array('output'=>$output->output),TRUE);
            $this->loadView($output);
        }
        
        function evaluacion($x = '',$y = ''){
            $crud = $this->crud_function($x,$y); 
            $crud->columns('id','cedula_evaluador','nombre_evaluador','telefono_evaluador','fecha_evaluacion','observaciones','estatus_evaluacion');
            //Condiciones
            if(is_numeric($x) && $y=='add'){
                $crud->field_type('centro_id','hidden',$x);
                $crud->unset_back_to_list();
            }else{
                $crud->set_relation('centro_id','centro','nombre_plantel');
            }            
           // $crud->unset_add();
            //Validaciones
            $crud->set_rules('cedula_evaluador','Cedula del Evaluador','required|numeric');
            //Displays
            $crud->display_as('cedula_evaluador','Cédula del Evaluador');
            $crud->display_as('nombre_evaluador','Nombre del Evaluador');
            $crud->display_as('telefono_evaluador','Telefono del Evaluador');
            $crud->display_as('fecha_evaluador','Fecha de Evaluación');
            $crud->display_as('gestion_instalacion_aire','Gestión de Instalación de Aires');
            $crud->display_as('distancia_piso_techo','Distancia del Piso al Techo');
            $crud->display_as('posee_aire_acondicionado','Cantidad de Aires Acondicionado');
            $crud->display_as('cantidad_aires_operativos','Cantidad de Aires Operativos');
            $crud->display_as('cantidad_aires_no_operativos','Cantidad de Aires No Operativos');
            $crud->display_as('centro_id','Centro');
            $crud->display_as('mts_cuadrados','Metros Cuadrados del Espacio')
                 ->display_as('operativo','Centro Operativo?');
          
            //Filtros por perfiles de usuarios
            //Fields Types
            if(in_array(self::__ADMINISTRADOR__,$this->user->grupos) || in_array(self::__DESARROLLADOR__,$this->user->grupos)){
                $crud->field_type('estatus_evaluacion','enum',array(
                    'En Revisión',
                    'Aprobado',
                    'Rechazado'
                ));
            }else{
                $crud->field_type('estatus_evaluacion','hidden','En Revisión');
            }
            if(in_array(self::__CORDINADOR__,$this->user->grupos) || in_array(self::__ADMINISTRADOR__,$this->user->grupos) || in_array(self::__DESARROLLADOR__,$this->user->grupos)){                
                $crud->field_type('cedula_evaluador','string',$this->user->cedula);
                $crud->field_type('nombre_evaluador','string',$this->user->nombres_apellidos);
                $crud->field_type('telefono_evaluador','string',$this->user->telefono);
                $crud->field_type('fecha_evaluacion','string',date("Y-m-d"));
                //Filtro
                if(
                        $crud->getParameters()=='list' && 
                        in_array(self::__CORDINADOR__,$this->user->grupos) && 
                        !in_array(self::__ADMINISTRADOR__,$this->user->grupos) && 
                        !in_array(self::__DESARROLLADOR__,$this->user->grupos)
                ){
                    $crud->where('estado_id',$this->user->estado_id);
                }
            }else{               
                $crud->where('cedula_evaluador',$this->user->cedula);
                $crud->field_type('cedula_evaluador','hidden',$this->user->cedula);
                $crud->field_type('nombre_evaluador','hidden',$this->user->nombres_apellidos);
                $crud->field_type('telefono_evaluador','hidden',$this->user->telefono);
                $crud->field_type('fecha_evaluacion','hidden',date("Y-m-d"));
            }
            $crud->field_type('tipo_solicitud','enum',array(
                'DOTACIÓN',
                'REDOTACIÓN PARCIAL',
                'REDOTACIÓN TOTAL'                
            ));

            
            $crud->field_type('filtraciones','enum',array(
                'Si',
                'No'
            ));
            $crud->field_type('gestion_instalacion_aire','enum',array(
                'Si',
                'No'
            ));
            $crud->field_type('paredes_agrietadas','enum',array(
                'Si',
                'No'
            ));
            $crud->field_type('operativo','enum',array(
                'Si',
                'No'
            ));
            $crud->field_type('cantidad_aires_operativos','hidden',0)
                 ->field_type('cantidad_aires_no_operativos','hidden',0)
                 ->field_type('posee_aire_acondicionado','hidden',0);
            
            //Filtro
            if(is_numeric($x)){
                $crud->where('centro_id',$x);
            }
            $crud->unset_delete()->unset_read()->unset_export()->unset_print();
            $crud->add_action('<i class="fa fa-leanpub"></i> Techos','',base_url('evaluacion/admin/techo_evaluacion').'/');
            $crud->add_action('<i class="fa fa-ellipsis-h"></i> Pisos','',base_url('evaluacion/admin/piso_evaluacion').'/');
            $crud->add_action('<i class="fa fa-delicious"></i> Paredes','',base_url('evaluacion/admin/paredes_evaluacion').'/');
            $crud->add_action('<i class="fa fa-window-restore"></i> Ventanas','',base_url('evaluacion/admin/ventanas_evaluacion').'/');
            $crud->add_action('<i class="fa fa-square"></i> Puertas','',base_url('evaluacion/admin/puertas_evaluacion').'/');
            $crud->add_action('<i class="fa fa-plug"></i> Tomas','',base_url('evaluacion/admin/tomas_corrientes_evaluacion').'/');
            $crud->add_action('<i class="fa fa-lightbulb-o"></i> Iluminación','',base_url('evaluacion/admin/iluminacion_evaluacion').'/');
            $crud->add_action('<i class="fa fa-font"></i> Aires','',base_url('evaluacion/admin/tipo_aire_acondicionado').'/');
            $crud->add_action('<i class="fa fa-user"></i> Tutores','',base_url('evaluacion/admin/tutor').'/');
            $crud->add_action('<i class="fa fa-address-book"></i> Requerimientos','',base_url('evaluacion/admin/requerimientos').'/');
            $crud->add_action('<i class="fa fa-rss"></i> Conectividad','',base_url('evaluacion/admin/conectividad_evaluacion').'/');
            $crud->add_action('<i class="fa fa-file-image-o"></i> Adm. Fotos','',base_url('evaluacion/admin/evaluacion_fotos').'/');
            $crud->add_action('<i class="fa fa-print"></i> exportar html','',base_url('reporte/rep/verReportes/2/html/evaluacion_id').'/');
            $crud->add_action('<i class="fa fa-print"></i> exportar pdf','',base_url('reporte/rep/verReportes/2/pdf/evaluacion_id').'/');
            $crud->add_action('<i class="fa fa-print"></i> exportar ods','',base_url('reporte/rep/verReportes/2/excel/evaluacion_id').'/');
            $crud->set_lang_string('insert_success_message','Sus datos fueron almacenados correctamente, <script>document.location.href="'.base_url('evaluacion/admin/techo_evaluacion').'/{id}/add"</script>');    
            $output = $crud->render();
            //$output->output = $this->load->view('centro',array('output'=>$output->output),TRUE);
            $this->loadView($output);

        }
        
        function techo_evaluacion($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);  
            
            if(is_numeric($x) && ($crud->getParameters()=='add' || $crud->getParameters()=='edit')){
                $crud->field_type('evaluacion_id','hidden',$x); 
                $crud->set_lang_string('insert_success_message','Sus datos fueron almacenados correctamente, puede seguir añadiendo o pulsar en siguiente paso para continuar con el registro <script>$("#btnext").show();</script>');
                $crud->unset_back_to_list();
            }else{
                $crud->set_relation('evaluacion_id','evaluacion','nombre_evaluador');
            }            
            //$crud->unset_add();
            $crud->set_subject('Techo');            
            $crud->display_as('evaluacion_id','Evaluación');
            $crud->set_relation('tipo_techo_id','tipo_techo','tipo_techo');
            $crud->display_as('tipo_techo_id','Tipo de Techo');
            $crud->set_relation('condicion_id','condicion','condicion');
            $crud->display_as('condicion_id','Condición');
            //Filtro
            if(is_numeric($x)){
                $crud->where('evaluacion_id',$x);
            }
            $output = $crud->render();
            $output ->title='Techo';
            
            
            $outputs = $output->output;
            $output->output = '<a href="'.base_url('evaluacion/admin/evaluacion/'.$this->get_centro($x)).'" class="btn btn-info">Volver al centro</a>';
            $output->output.= '<a id="btnext" style="display:none" href="'.base_url('evaluacion/admin/piso_evaluacion/'.$x.'/add').'" class="btn btn-success">Ir al siguiente paso <i class="fa fa-chevron-right"></i></a>';
            $output->output.= $outputs;
            
            $this->loadView($output);

        }
        
        function piso_evaluacion($x = '',$y = ''){
            $crud = $this->crud_function($x,$y); 
            
            if(is_numeric($x) && ($crud->getParameters()=='add' || $crud->getParameters()=='edit')){
                $crud->field_type('evaluacion_id','hidden',$x);
                $crud->set_lang_string('insert_success_message','Sus datos fueron almacenados correctamente, puede seguir añadiendo o pulsar en siguiente paso para continuar con el registro <script>$("#btnext").show();</script>');
                $crud->unset_back_to_list();
            }else{
                $crud->set_relation('evaluacion_id','evaluacion','nombre_evaluador');
            }  
            //$crud->unset_add();
            $crud->set_subject('Pisos');                       
            $crud->display_as('evaluacion_id','Evaluación');
            $crud->set_relation('tipo_piso_id','tipo_piso','tipo_piso');
            $crud->display_as('tipo_piso_id','Tipo de Pisos');
            $crud->set_relation('condicion_id','condicion','condicion');
            $crud->display_as('condicion_id','Condición');
            //Filtro
            if(is_numeric($x)){
                $crud->where('evaluacion_id',$x);
            }
            $output = $crud->render();
            $output ->title='Pisos';
            $outputs = $output->output;
            $output->output = '<a href="'.base_url('evaluacion/admin/evaluacion/'.$this->get_centro($x)).'" class="btn btn-info">Volver al centro</a>';
            $output->output.= '<a id="btnext" style="display:none" href="'.base_url('evaluacion/admin/paredes_evaluacion/'.$x.'/add').'" class="btn btn-success">Ir al siguiente paso <i class="fa fa-chevron-right"></i></a>';
            $output->output.= $outputs;
            $this->loadView($output);

        }
        
        function paredes_evaluacion($x = '',$y = ''){
            $crud = $this->crud_function($x,$y); 
            if(is_numeric($x) && ($crud->getParameters()=='add' || $crud->getParameters()=='edit')){
                $crud->field_type('evaluacion_id','hidden',$x);
                $crud->set_lang_string('insert_success_message','Sus datos fueron almacenados correctamente, puede seguir añadiendo o pulsar en siguiente paso para continuar con el registro <script>$("#btnext").show();</script>');
                $crud->unset_back_to_list();
            }else{
                $crud->set_relation('evaluacion_id','evaluacion','nombre_evaluador');
            } 
           // $crud->unset_add();
            $crud->set_subject('Paredes');
            $crud->display_as('evaluacion_id','Evaluación');
            $crud->set_relation('tipo_paredes_id','tipo_paredes','tipo_paredes');
            $crud->display_as('tipo_paredes_id','Tipo de Paredes');
            $crud->set_relation('condicion_id','condicion','condicion');
            $crud->display_as('condicion_id','Condición');
            //Filtro
            if(is_numeric($x)){
                $crud->where('evaluacion_id',$x);
            }
            $output = $crud->render();            
            $output ->title='Paredes';
            $outputs = $output->output;
            $output->output = '<a href="'.base_url('evaluacion/admin/evaluacion/'.$this->get_centro($x)).'" class="btn btn-info">Volver al centro</a>';
            $output->output.= '<a id="btnext" style="display:none" href="'.base_url('evaluacion/admin/ventanas_evaluacion/'.$x.'/add').'" class="btn btn-success">Ir al siguiente paso <i class="fa fa-chevron-right"></i></a>';
            $output->output.= $outputs;
            $this->loadView($output);

        }
        
        function ventanas_evaluacion($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);  
            if(is_numeric($x) && ($crud->getParameters()=='add' || $crud->getParameters()=='edit')){
                $crud->field_type('evaluacion_id','hidden',$x);
                $crud->set_lang_string('insert_success_message','Sus datos fueron almacenados correctamente, puede seguir añadiendo o pulsar en siguiente paso para continuar con el registro <script>$("#btnext").show();</script>');
                $crud->unset_back_to_list();
            }else{
                $crud->set_relation('evaluacion_id','evaluacion','nombre_evaluador');
            } 
            //$crud->unset_add();
            $crud->set_subject('Ventanas');            
            $crud->display_as('evaluacion_id','Evaluación');
            $crud->set_relation('tipo_ventanas_id','tipo_ventanas','tipo_ventanas');
            $crud->display_as('tipo_ventanas_id','Tipo de Ventanas');
            $crud->display_as('cantidad_buenos','Cantidad de Buenos');
            $crud->display_as('cantidad_regular','Cantidad de Regulares');
            $crud->display_as('cantidad_malos','Cantidad de Malos');
            $crud->display_as('posee_rejas_si','Cantidad de Ventanas Con Rejas');
            $crud->display_as('posee_rejas_no','Cantidad de Ventanas Sin Rejas');
            //Filtro
            if(is_numeric($x)){
                $crud->where('evaluacion_id',$x);
            }
            $output = $crud->render();
            $output ->title='Ventanas';
            $outputs = $output->output;
            $output->output = '<a href="'.base_url('evaluacion/admin/evaluacion/'.$this->get_centro($x)).'" class="btn btn-info">Volver al centro</a>';
            $output->output.= '<a id="btnext" style="display:none" href="'.base_url('evaluacion/admin/puertas_evaluacion/'.$x.'/add').'" class="btn btn-success">Ir al siguiente paso <i class="fa fa-chevron-right"></i></a>';
            $output->output.= $outputs;
            $this->loadView($output);

        }
        function puertas_evaluacion($x = '',$y = ''){
            $crud = $this->crud_function($x,$y); 
            if(is_numeric($x) && ($crud->getParameters()=='add' || $crud->getParameters()=='edit')){
                $crud->field_type('evaluacion_id','hidden',$x);
                $crud->set_lang_string('insert_success_message','Sus datos fueron almacenados correctamente, puede seguir añadiendo o pulsar en siguiente paso para continuar con el registro <script>$("#btnext").show();</script>');
                $crud->unset_back_to_list();
            }else{
                $crud->set_relation('evaluacion_id','evaluacion','nombre_evaluador');
            } 
           // $crud->unset_add();
            $crud->set_subject('Puertas');
            $crud->display_as('evaluacion_id','Evaluación');
            $crud->set_relation('tipo_puerta_id','tipo_puerta','tipo_puerta');
            $crud->display_as('tipo_puerta_id','Tipo de Puertas');
            $crud->display_as('cantidad_buenos','Cantidad de Buenos');
            $crud->display_as('cantidad_regular','Cantidad de Regulares');
            $crud->display_as('cantidad_malos','Cantidad de Malos');
            $crud->display_as('posee_rejas_si','Cantidad Puertas Con Rejas');
            $crud->display_as('posee_rejas_no','Cantidad Puertas Sin Rejas');
            //Filtro
            if(is_numeric($x)){
                $crud->where('evaluacion_id',$x);
            }
            $output = $crud->render();
            $output ->title='Puertas';
            $outputs = $output->output;
            $output->output = '<a href="'.base_url('evaluacion/admin/evaluacion/'.$this->get_centro($x)).'" class="btn btn-info">Volver al centro</a>';
            $output->output.= '<a id="btnext" style="display:none" href="'.base_url('evaluacion/admin/tomas_corrientes_evaluacion/'.$x.'/add').'" class="btn btn-success">Ir al siguiente paso <i class="fa fa-chevron-right"></i></a>';
            $output->output.= $outputs;
            $this->loadView($output);

        }
        
        function tomas_corrientes_evaluacion($x = '',$y = ''){            
            $crud = $this->crud_function($x,$y); 
            if(is_numeric($x) && ($crud->getParameters()=='add' || $crud->getParameters()=='edit')){
                $crud->field_type('evaluacion_id','hidden',$x);
                $crud->set_lang_string('insert_success_message','Sus datos fueron almacenados correctamente, puede seguir añadiendo o pulsar en siguiente paso para continuar con el registro <script>$("#btnext").show();</script>');
                $crud->unset_back_to_list();
            }else{
                $crud->set_relation('evaluacion_id','evaluacion','nombre_evaluador');
            }  
           // $crud->unset_add();
            $crud->set_subject('Tomas De Corriente');
            $crud->display_as('evaluacion_id','Evaluación');
            $crud->set_relation('tomas_corrientes_id','tomas_corrientes','tomas_corrientes');
            
            $crud->display_as('tomas_corrientes_id','Tomas De Corriente');
            $crud->display_as('cantidad_buenos','Cantidad de Buenos');
            $crud->display_as('cantidad_regular','Cantidad de Regulares');
            $crud->display_as('posee_tablero_electrico','Posee tablero eléctrico independiente');
            $crud->display_as('cantidad_malos','Cantidad de Malos');
            $crud->display_as('tomas_corrientes_empotrados_si','Cantidad de Tomas Corrientes Empotrados');
            $crud->display_as('tomas_corrientes_empotrados_no','Cantidad de Tomas Corrientes Sin Empotrar');
            
            $crud->field_type('posee_tablero_electrico','enum',array(
                'Si',
                'No'
            ));
            
             $crud->field_type('posee_espacio_tablero','enum',array(
                'Si',
                'No'
            ));
             
              $crud->field_type('mantenimiento_preventivo','enum',array(
                'Si',
                'No'
            ));
              
               $crud->field_type('mantenimiento_correctivo','enum',array(
                'Si',
                'No'
            ));
               //Filtro
            if(is_numeric($x)){
                $crud->where('evaluacion_id',$x);
            }
            $output = $crud->render();
            $output ->title='Tomas De Corriente';
            $outputs = $output->output;
            $output->output = '<a href="'.base_url('evaluacion/admin/evaluacion/'.$this->get_centro($x)).'" class="btn btn-info">Volver al centro</a>';
            $output->output.= '<a id="btnext" style="display:none" href="'.base_url('evaluacion/admin/iluminacion_evaluacion/'.$x.'/add').'" class="btn btn-success">Ir al siguiente paso <i class="fa fa-chevron-right"></i></a>';
            $output->output.= $outputs;
            $this->loadView($output);

        }
        
        function iluminacion_evaluacion($x = '',$y = ''){                
            $crud = $this->crud_function($x,$y);
            if(is_numeric($x) && ($crud->getParameters()=='add' || $crud->getParameters()=='edit')){
                $crud->field_type('evaluacion_id','hidden',$x);
                $crud->set_lang_string('insert_success_message','Sus datos fueron almacenados correctamente, puede seguir añadiendo o pulsar en siguiente paso para continuar con el registro <script>$("#btnext").show();</script>');
                $crud->unset_back_to_list();
            }else{
                $crud->set_relation('evaluacion_id','evaluacion','nombre_evaluador');
            }  
            //$crud->unset_add();
            $crud->set_subject('Iluminación');            
            $crud->display_as('evaluacion_id','Evaluación');
            $crud->display_as('tipo_iluminacion_id','Tipo de Iluminación');
            $crud->field_type('posee_proteccion','enum',array(
                'Si',
                'No'
            ));
            //Filtro
            if(is_numeric($x)){
                $crud->where('evaluacion_id',$x);
            }
            $output = $crud->render();
            $output ->title='Iluminación';
            $outputs = $output->output;
            $output->output = '<a href="'.base_url('evaluacion/admin/evaluacion/'.$this->get_centro($x)).'" class="btn btn-info">Volver al centro</a>';
            $output->output.= '<a id="btnext" style="display:none" href="'.base_url('evaluacion/admin/tipo_aire_acondicionado/'.$x.'/add').'" class="btn btn-success">Ir al siguiente paso <i class="fa fa-chevron-right"></i></a>';
            $output->output.= $outputs;
            $this->loadView($output);

        }
        
         function tipo_aire_acondicionado($x = '',$y = ''){                         
            $crud = $this->crud_function($x,$y);          
            if(is_numeric($x) && ($crud->getParameters()=='add' || $crud->getParameters()=='edit')){
                $crud->field_type('evaluacion_id','hidden',$x);
                $crud->set_lang_string('insert_success_message','Sus datos fueron almacenados correctamente, puede seguir añadiendo o pulsar en siguiente paso para continuar con el registro <script>$("#btnext").show();</script>');
                $crud->unset_back_to_list();
            }else{
                $crud->set_relation('evaluacion_id','evaluacion','nombre_evaluador');
            }  
            $crud->set_subject('Aire Acondicionado');                       
            $crud->display_as('evaluacion_id','Evaluación');
            $crud->display_as('tipo_aire_id','Tipo de Aire');
            $crud->display_as('mantenimiento_preventivo','Posee Mantenimiento Preventivo?')
                 ->field_type('mantenimiento_preventivo','true_false',array('0'=>'NO','1'=>'SI'));
            $crud->display_as('aire_acondicionado_id','Especificación del Aire Acondicionado');
            //Filtro
            if(is_numeric($x)){
                $crud->where('evaluacion_id',$x);
            }
            $output = $crud->render();
            $output ->title='Aire Acondicionado';
            $outputs = $output->output;
            $output->output = '<a href="'.base_url('evaluacion/admin/evaluacion/'.$this->get_centro($x)).'" class="btn btn-info">Volver al centro</a>';
            $output->output.= '<a id="btnext" style="display:none" href="'.base_url('evaluacion/admin/conectividad_evaluacion/'.$x.'/add').'" class="btn btn-success">Ir al siguiente paso <i class="fa fa-chevron-right"></i></a>';
            $output->output.= $outputs;
            $this->loadView($output);

        }
        
        function conectividad_evaluacion($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);
            if(is_numeric($x) && ($crud->getParameters()=='add' || $crud->getParameters()=='edit')){
                $crud->field_type('evaluacion_id','hidden',$x);                
                $crud->set_lang_string('insert_success_message','Sus datos fueron almacenados correctamente, puede seguir añadiendo o pulsar en siguiente paso para continuar con el registro <script>$("#btnext").show();</script>');
                $crud->unset_back_to_list();
            }else{
                $crud->set_relation('evaluacion_id','evaluacion','nombre_evaluador');               
            }
            $crud->field_type('servicio_internet','enum',array(
                'Si',
                'No'
            ));
             $crud->field_type('servicio_wifi_libre','enum',array(
                'Si',
                'No'
            ));
             $crud->where('evaluacion_id',$x);
             
            $output = $crud->render();
            //$output->output = $this->load->view('tutor',array('output'=>$output->output),TRUE);
            $outputs = $output->output;
            $output->output = '<a href="'.base_url('evaluacion/admin/evaluacion/'.$this->get_centro($x)).'" class="btn btn-info">Volver al centro</a>';
            $output->output.= '<a id="btnext" style="display:none" href="'.base_url('evaluacion/admin/tutor/'.$x.'/add').'" class="btn btn-success">Ir al siguiente paso <i class="fa fa-chevron-right"></i></a>';
            $output->output.= $outputs;
            $this->loadView($output);

        }
        
        function tutor($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);
            if(is_numeric($x) && ($crud->getParameters()=='add' || $crud->getParameters()=='edit')){
                $crud->field_type('evaluacion_id','hidden',$x);
                $crud->field_type('telefono','telefono',array('placeholder'=>'(0999)999-9999'));
                //$crud->set_lang_string('insert_success_message','Sus datos fueron almacenados correctamente, <script>document.location.href="'.base_url('sigecet/').'/'.$x.'/add"</script>');
                $crud->set_lang_string('insert_success_message','Sus datos fueron almacenados correctamente, puede seguir añadiendo o pulsar en siguiente paso para continuar con el registro <script>$("#btnext").show();</script>');
                $crud->unset_back_to_list();
            }else{
                $crud->set_relation('evaluacion_id','evaluacion','nombre_evaluador');
               // $crud->set_lang_string('insert_success_message','Sus datos fueron almacenados correctamente, <script>document.location.href="'.base_url('').'"</script>');                               
            }
            $crud->field_type('dependencia_nominal','enum',array(
                'Fundabit',
                'MPPE'
            ));
            if($crud->getParameters()=='add'){
                $crud->set_rules('cedula','Cedula','required|numeric|is_unique[tutor.cedula]');
            }
            //Filtro
            if(is_numeric($x)){
                $crud->where('evaluacion_id',$x);
            }
            $output = $crud->render();
            $output->output = $this->load->view('tutor',array('output'=>$output->output),TRUE);
            $outputs = $output->output;
            $output->output = '<a href="'.base_url('evaluacion/admin/evaluacion/'.$this->get_centro($x)).'" class="btn btn-info">Volver al centro</a>';
            $output->output.= '<a id="btnext" style="display:none" href="'.base_url('evaluacion/admin/requerimientos/'.$x.'/add').'" class="btn btn-success">Ir al siguiente paso <i class="fa fa-chevron-right"></i></a>';
            $output->output.= $outputs;
            $this->loadView($output);

        }
        
        function requerimientos($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);
            if(is_numeric($x) && ($crud->getParameters()=='add' || $crud->getParameters()=='edit')){
                $crud->field_type('evaluacion_id','hidden',$x);
                //$crud->set_lang_string('insert_success_message','Sus datos fueron almacenados correctamente, <script>document.location.href="'.base_url('sigecet/').'/'.$x.'/add"</script>');
                $crud->set_lang_string('insert_success_message','Sus datos fueron almacenados correctamente, puede seguir añadiendo o pulsar en siguiente paso para continuar con el registro <script>$("#btnext").show();</script>');
                $crud->unset_back_to_list();
            }else{
                $crud->set_relation('evaluacion_id','evaluacion','nombre_evaluador');
               // $crud->set_lang_string('insert_success_message','Sus datos fueron almacenados correctamente, <script>document.location.href="'.base_url('').'"</script>');
               
                
            }
            $crud->where('evaluacion_id',$x);
            $output = $crud->render();
            $outputs = $output->output;
            $output->output = '<a href="'.base_url('evaluacion/admin/evaluacion/'.$this->get_centro($x)).'" class="btn btn-info">Volver al centro</a>';
            $output->output.= '<a id="btnext" style="display:none" href="'.base_url('evaluacion/admin/evaluacion_fotos/'.$x).'" class="btn btn-success">Ir al siguiente paso <i class="fa fa-chevron-right"></i></a>';
            $output->output.= $outputs;
            $this->loadView($output);

        }
        
        
        function evaluacion_fotos($x = ''){
            $this->load->library('image_crud');
            $crud = new image_CRUD();
            $crud->set_table('evaluacion_fotos')
                 ->set_relation_field('evaluacion_id')
                 ->set_image_path('img/evaluaciones')
                 ->set_ordering_field('orden')
                 ->set_subject('Fotos')
                 ->set_url_field('foto')
                 ->set_title_field('descripcion');
            $crud->module = 'evaluacion';         
            $output = $crud->render();
            $outputs = $output->output;
            $output->output = '<a href="'.base_url('evaluacion/admin/evaluacion/'.$this->get_centro($x)).'" class="btn btn-info">Volver al centro</a>';
            $output->output.= $outputs;
            $this->loadView($output);
            
        }
      
        
    }
?>
