<?php

class User extends CI_Model{
        var $log = false;

        function __construct(){
                parent::__construct();
                if(!empty($_SESSION['user'])){
                        $this->log = TRUE;
                        $this->set_variables();
                        $this->createKeyChain();
                }                
        }
        
        function createKeyChain(){
            unset($this->keychain);
            unset($_SESSION['keychain']);
            if(empty($this->keychain)){
                if(empty($_SESSION['keychain'])){
                    $keychain = array();
                    $permisos = $this->getAccess('funciones.nombre');
                    foreach($permisos->result() as $f){
                        if(!in_array($f->nombre,$keychain)){
                            $keychain[] = $f->nombre;
                        }
                    }
                    $this->keychain = $keychain;
                    $_SESSION['keychain'] = json_encode($this->keychain);
                }else{                    
                    $this->keychain = json_decode($_SESSION['keychain']);
                }
            }
            else{
                if(!is_array($this->keychain)){
                    $this->keychain = json_decode($this->keychain);
                }
            }
        }
        
        function filtrarMenu($menu){
            foreach($menu as $n=>$m){
                $a = array();
                foreach($m as $ff=>$f){
                    if(is_array($f)){
                        $a[] = $this->filtrarMenu(array($ff=>$f));
                    }else{
                        $funcion = explode('/',$f);
                        if(in_array($funcion[count($funcion)-1],$this->keychain)){
                            $a[] = $f;
                        }
                    }
                }
                $menu[$n] = $a;
            }            
            return $menu;
        }
        
        function getItemMenu($ruta,$label = ''){
            $funcion = explode('/',$ruta);
            $label = empty($label)?  ucfirst(str_replace('_',' ',$funcion)):$label;
            if(in_array($funcion[count($funcion)-1],$this->keychain)){
                return '<li><a href="'.base_url($ruta).'">'.$label.'</a></li>';
            }else{
                return '';
            }
        }

        function login($user,$pass)
        {                
                $ldapconn = ldap_connect('10.10.71.30') or die("No es posible conectar");
                ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
                if ($ldapconn) {
                    //Buscamos su direccion        
                    $dn = 'cn=conexion,dc=fundabit,dc=gob,dc=ve';        
                    $password = "fund4b1t14";
                    $ldapbind = @ldap_bind($ldapconn,$dn,$password);
                    if($ldapbind){
                        $usuario = $user;
                        $password = $pass;            
                        $result = ldap_search($ldapconn,"dc=fundabit,dc=gob,dc=ve", "uid=$usuario");
                        $entries = ldap_get_entries($ldapconn, $result);
                        $direccion = $entries[0]["ou"][0];
                        //Autenticamos al usuario con sus credenciales
                        $dn = "uid=$usuario,ou=$direccion,ou=Usuarios,dc=fundabit,dc=gob,dc=ve";
                        $ldapbind = @ldap_bind($ldapconn,$dn,$password);
                        if($ldapbind){
                            $r = array();
                            $r['id'] = $entries[0]["carlicense"][0];
                            $r['cedula'] = $entries[0]["carlicense"][0];
                            $r['nombres_apellidos'] = $entries[0]["givenname"][0];
                            $r['mail'] = $entries[0]["mail"][0];
                            //Traemos de sigesp estado_id y telefono
                            $sigesp = $this->sigesp->get_where('sno_personal',array('cedper'=>$r['id']));
                            if($sigesp->num_rows()>0){
                                $r['estado_id'] = $sigesp->row()->codest;
                                $r['telefono'] = $sigesp->row()->telmovper;
                            }else{
                                $_SESSION['msj'] = "Usuario no registrado en nomina";
                                return false;
                                die();
                            }
                            
                            $this->getParameters((object)$r);                            
                            return true;
                        }
                        else{
                            $_SESSION['msj'] = "Credenciales incorrectas";
                            return false;
                        }
                    } else {
                        $_SESSION['msj'] = "Error en consulta con el usuario administrador";
                        return false;
                    }
                }
        }

        function login_short($id)
        {
                $this->db->select('usuario.*, usuario.id as id');
                $this->getParameters($this->db->get_where('usuario',array('id'=>$id)));
        }

        function getParameters($row)
        {                        
            foreach($row as $n=>$r){
                $_SESSION[$n] = $r;
            }
            $_SESSION['user'] = $_SESSION['id'];
            //Creamos la variable grupos
            $_SESSION['grupos'] = array();
            foreach($this->db->get_where('user_group',array('user'=>$row->id))->result() as $g){
                $_SESSION['grupos'][] = $g->grupo;
            }
            $this->set_variables();
        }

        function set_variables(){
            foreach($_SESSION as $n=>$x){
                $this->$n = $x;
            }
            $_SESSION['user'] = $_SESSION['id'];
            $this->user = $_SESSION['user'];
        }
        
        function setVariable($nombre,$valor){
            $_SESSION[$nombre] = $valor;
            $this->set_variables();
        }

        function unlog()
        {
                session_unset();
        }
        
        function getOperations($operation){
            switch($operation){
                case 'list':
                case 'read':
                case 'export':
                case 'print':
                case 'success':
                    return 'lectura';
                break;
                case 'add':
                case 'edit':
                case 'delete':
                    return 'escritura';
                break;
                default: return 'lectura';
                 break;
            }
        }
        
        
        function getAccess($select,$where = array(), $user = ''){
            if(!empty($_SESSION['user'])){
                $this->db->select($select);                
                $this->db->join('grupos','grupos.id = user_group.grupo');
                $this->db->join('funcion_grupo','funcion_grupo.grupo = grupos.id');
                $this->db->join('funciones','funciones.id = funcion_grupo.funcion');
                $where['user_group.user'] = empty($user)?$this->user:$user;
                $access = $this->db->get_where('user_group',$where);
                return $access;
            }
            else {
                header("Location:".base_url());
                exit;
            }
        }
        public $allrequired = array(
            'getMunicipios',
            'getParroquias'
        );
        function hasAccess(){
            $funcion = $this->router->fetch_method();            
            if(!in_array($funcion,$this->allrequired)){
                $this->load->library('ajax_grocery_crud');
                $crud = new ajax_grocery_crud();            
                $fun = $this->getOperations($crud->getParameters());//Almacenar si es lectura o escritura  
                $permisos = $this->getAccess('grupos.*',array('funciones.nombre'=>$funcion,$fun=>1));                            
                return $permisos->num_rows()>0 || $this->router->fetch_class()=='panel'?TRUE:FALSE;
            }else{
                return true;
            }
        }
}
?>
